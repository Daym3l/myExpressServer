/**
 * @Created with WebStorm.
 * @Author: Daymel
 * @Date: 17/3/2018
 * @Time: 22:34
 * @Version:1.0
 */
const express = require('express');
const http = require('http');
const path = require('path');
const m_url_register = require('morgan');
const app = express();
const route = require('./routes');
const routeApi = require('./api_routes');

//settings
app.set('appName', 'NODE SERVER EXPRESS');
app.set('dirServer', "http://localhost:3000");
app.set('auth', "Daymel Machado Cabrera");
app.set('view engine', 'pug');
app.set('views', __dirname + '/views');

//middlewares
app.use(m_url_register('short'));
app.use(express.static(path.join(__dirname, 'public')));

// Routing
app.use(route);
app.use('/api',routeApi);

// Handle 404
app.use((req, res) => {
    res.status(400);
    res.render('404.pug', { title: '404: File Not Found' });
});

// Handle 500
app.use((error, req, res, next) => {
    res.status(500);
    res.render('500.pug', { title: '500: Internal Server Error', error: error });
});


app.set('port', process.env.PORT || 3000);
app.listen(3000);
console.log("Name*: " + app.get('appName'));
console.log("Url*: " + app.get('dirServer'));
console.log("Auth*: " + app.get('auth'));
console.log("------------LOGS-------------------")



